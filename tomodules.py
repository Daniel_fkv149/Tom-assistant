# -*- coding: utf-8 -*-
import tomodules
import pyrender
import community_scripts
import game
# external modules:
import datetime
import webbrowser
import os
from os.path import exists
import time
from math import pi
import linecache
import pathlib
import string
import secrets
from random import randint
from getpass import getuser
__version__ = "v1.3.0"

time_pause = 0.05
var_rep = []
class Var_info:
    def __init__(self, name, description, vtype):
        self.name = name
        self.description = description
        self.type = vtype
        var_rep.append(self)
    def info(self):
        print(f"""
VAR INFO:
    var name: {self.name}
    var type: {self.type}\n
    description:\n
        {self.description}
""")
def exit():
    tomodules.clear_terminal()
    print("\nQUITTING...")
    f = open("history.txt", "w+")
    for x in range(len(Chat.history)):
        h = Chat.history[x]
        f.write(h + ";")
    f.close()
    [p.unlink() for p in pathlib.Path('.').rglob('*.py[co]')]
    [p.rmdir() for p in pathlib.Path('.').rglob('__pycache__')]
    quit()

def clear_terminal():
    if os.name =="nt":
        os.system("cls")
    else:
        os.system("clear")

def banner():
    try:
        time.sleep(2.3)
        clear_terminal()
        if os.name =="nt":
            os.system("color 01")
        else:
            pass
        print("\n\n          TTTTTTTTTTTTTTTTTTTTTTT                                      ")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 02")
        else:
            pass
        print("          T:::::::::::::::::::::T                                      ")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 03")
        else:
            pass
        print("          T:::::::::::::::::::::T                                      ")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 04")
        else:
            pass
        print("          T:::::TT:::::::TT:::::T                                      ")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 05")
        else:
            pass
        print("          TTTTTT  T:::::T  TTTTTTooooooooooo      mmmmmmm    mmmmmmm   ")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 06")
        else:
            pass
        print("                  T:::::T      oo:::::::::::oo  mm:::::::m  m:::::::mm ")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 07")
        else:
            pass
        print("                  T:::::T     o:::::::::::::::om::::::::::mm::::::::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 08")
        else:
            pass
        print("                  T:::::T     o:::::ooooo:::::om::::::::::::::::::::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 09")
        else:
            pass
        print("                  T:::::T     o::::o     o::::om:::::mmm::::::mmm:::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 0B")
        else:
            pass
        print("                  T:::::T     o::::o     o::::om::::m   m::::m   m::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 0C")
        else:
            pass
        print("                  T:::::T     o::::o     o::::om::::m   m::::m   m::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 0D")
        else:
            pass
        print("                  T:::::T     o::::o     o::::om::::m   m::::m   m::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 0E")
        else:
            pass
        print("                TT:::::::TT   o:::::ooooo:::::om::::m   m::::m   m::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 01")
        else:
            pass
        print("                T:::::::::T   o:::::::::::::::om::::m   m::::m   m::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 02")
        else:
            pass
        print("                T:::::::::T    oo:::::::::::oo m::::m   m::::m   m::::m")
        time.sleep(time_pause)
        if os.name =="nt":
            os.system("color 03")
        else:
            pass
        print("                TTTTTTTTTTT      ooooooooooo   mmmmmm   mmmmmm   mmmmmm")
        time.sleep(0.2)
        if os.name =="nt":
            os.system("color 0F")
        else:
            pass
        input("\n\n\n\nPress Enter to continue...")
        clear_terminal()
    except KeyboardInterrupt:
        if os.name == "nt":
            os.system("color 0F")
        else:
            pass
        clear_terminal()

#############################################################################

class Chat:
    idowru = randint(0, 3)
    fromsavehistory = True
    browser = True
    togglebrowser = None
    tellmore = 0
    hello = randint(0, 4)
    errornum = 0
    consterrornum = 0
    history = []
    togglebanner = None
    togglehistory = None
    globalkeephistory = True
    keephistory = True
    k_water = True
    k_mushrooms = True
    k_owru = True
    k_math = True
    k_quiz = True
    nk_look = True
    nk_browser = True
    k_browser = True
    k_lol = True
    k_nyan = True
    k_from = True
    k_ugly = True
    kindness = 5
    repeatedword = None
    repeatedtimes = 0

# init var info (access by the debug menu):
    kindness_inf = Var_info("kindness", "Var used to change the answers of Tom depending on the kindness of your questions.", "int")
    repeatedword_inf = Var_info("repeatedword", "Var used to store an unknown word when repeated more than 3 times in the chat.", "None/str")
    globalkeephistory_inf = Var_info("globalkeephistory", "Var used to define if the inputs shall be loged in the history.", "bool")

    def __init__(self):
        self.tom()

    def open(self, link):
        if self.browser == True:
            webbrowser.open_new(link)
        else:
            print(f"Browser openning disabled.\nurl: {link}")

    def settings(self):
        clear_terminal()
        try:
            f = open("settings.conf", "r")
        except:
            f = open("settings.conf", "w+")
            f.write("startup_banner:True")
        f.close()
        bolbanner = linecache.getline("settings.conf", 1)
        if bolbanner == "startup_banner:True" or bolbanner == "startup_banner:True\n":
            self.togglebanner = "disable"
        else:
            self.togglebanner = "enable"

        if self.globalkeephistory:
            self.togglehistory = "disable"
        else:
            self.togglehistory = "enable"

        if self.browser == True:
            self.togglebrowser = "disable"
        else:
            self.togglebrowser = "enable"

        settings = input("""
        Settings:

            1- """ + self.togglehistory + """ history
            2- enable/disable banner display on start (enabled by default)
            3- """ + self.togglebrowser + """ browser openning

            B- Back
>> """)
        if settings == "1":
            if self.globalkeephistory == True:
                self.globalkeephistory = False
                self.keephistory = False
                for x in range(len(self.history)):
                    a = 0
                    self.history.pop(a)
                    a = a + 1
            else:
                self.globalkeephistory = True
            clear_terminal()
            self.settings()
        elif settings == "2":
            f = open("settings.conf", "w+")
            if self.togglebanner == "disable":
                f.write("startup_banner:False")
            else:
                f.write("startup_banner:True")
            clear_terminal()
            self.settings()
        elif settings == "3":
            if self.browser == True:
                self.browser = False
            else:
                self.browser = True
            clear_terminal()
            self.settings()
        elif settings == "B":
            clear_terminal()
        else:
            clear_terminal()
            self.settings()

    def debug(self):
        while True:
            command = input(f"\n[{getuser()}@dbg]$> ")
            command_s = command.split(" ")
            if command == "exit" or command == "back" or command == "quit":
                break
            elif command == "help" or command == "?":
                print("""
    Symbols:    - > available      * > not available

        - vars / variables: display interesting variables values
        - info [var_name]: display info about a variable (use, type...)
        * add [var_name] [int]: add int value to a var of int type (var=int+var)
        - shell [command]: use the cmd / terminal
        - reload: reload the program, useful when editing the program scripts.
""")
            elif command == "variables" or command == "vars" or command == "print vars":
                print(f"kindness: {self.kindness}")
                print(f"globalkeephistory: {self.globalkeephistory}")
                print(f"repeatedword: {self.repeatedword}")
            elif command == "reload":
                os.system("python3 script.py")
                exit()
            elif command == "clear" or command == "cls":
                clear_terminal()
            elif command == "" or command == " ":
                pass
            elif command_s[0] == "info":
                if len(command_s) == 2:
                    for x in range(len(var_rep)):
                        if command_s[1] == var_rep[x].name:
                            notfound = False
                            var_rep[x].info()
                            break
                        else:
                            notfound = True
                    if notfound:
                        print(f"[ERROR]: var {command_s[1]} is not indexed.")
                    else:
                        pass
                else:
                    print("[ERROR]: wrong argument amount.")
            elif command_s[0] == "add":
                print("NOT AVAILABLE")
            elif command_s[0] == "shell":
                command_s.pop(0)
                execute = ""
                for x in range(len(command_s)):
                    execute = execute + command_s[x] + " "
                os.system(execute)
            else:
                print("Command not found.")

    def win_color_text(self):
        clear_terminal()
        color = input("""
        Select the text color:
            1- white
            2- green
            3- grey
            4- red
            5- yellow
            6- purple
            7- blue
            8- light blue
            R- reset
            99- Back

>> """)
        if color == "1":
            os.system("color 07")
            self.win_color_text()
        elif color == "2":
            os.system("color 0A")
            self.win_color_text()
        elif color == "3":
            os.system("color 08")
            self.win_color_text()
        elif color == "4":
            os.system("color 04")
            self.win_color_text()
        elif color == "5":
            os.system("color 06")
            self.win_color_text()
        elif color == "6":
            os.system("color 05")
            self.win_color_text()
        elif color == "7":
            os.system("color 01")
            self.win_color_text()
        elif color == "8":
            os.system("color 09")
            self.win_color_text()
        elif color == "R" or color == "r":
            os.system("color 0F")
            self.win_color_text()
        elif color == "99":
            clear_terminal()
        else:
            clear_terminal()
            self.win_color_text()
    def get_average(self):
        num_list = []
        launched = True
        while launched == True:
            print("To stop, type $OK.")
            print(f"Amount of numbers: {len(num_list)}.")
            add_num = input("\n Add a number: ")
            if add_num == "$OK":
                launched = False
            else:
                try:
                    clear_terminal()
                    num_list.append(float(add_num))
                except:
                    clear_terminal()
                    print("[x] ERROR: Input must be a number.\n")
        average = sum(num_list) / len(num_list)
        print(f"Average: {average}")

    def get_circle_circumference(self):
        mesure_unit = input("\nmesuring unit (cm, m...): ")
        circle_radius_str = input(f"circle radius in {mesure_unit}: ")
        try:
            circle_radius = float(circle_radius_str)
            circumf = circle_radius * 2 * pi
            print(f"\ncircumference of the circle: {circumf} {mesure_unit}")
        except:
            print("[x] ERROR: Input must be a number.")
            self.get_circle_circumference()

    def get_sci_number(self):
        number_str = input("\nEnter the number you want to convert in scientific number: ")
        try:
            number = float(number_str)
            exponent = 0
            if (number < 1):
                while (number < 1):
                    number = number * 10
                    exponent = exponent - 1
            if (number > 1):
                while (number > 10):
                    number = number / 10
                    exponent = exponent + 1
            print(f"\n{number} x 1O exponent : {exponent}.")
        except:
            print("[x] ERROR: Input must be a number.")
            self.get_sci_number()

    def get_ftemp(self):
        wind_speed_str = input("\nwind speed in km/h: ")
        temp_str = input("\nreal temperature Celsius: ")
        try:
            wind_speed = float(wind_speed_str)
            temp = float(temp_str)
            ftemp_equation = 13.12 + (0.6215 * temp) + (0.3965 * temp - 11.37) * pow(wind_speed, 0.16)
            print(f"\nThe felt temperature is: {ftemp_equation}")
        except:
            print("[x] ERROR: Input must be a number.")
            self.get_ftemp()

    def fahrenheit_to_celsius(self):
        tf_str = input("\nTemperature in Fahrenheit: ")
        try:
            tf = float(tf_str)
            tmp_celsius = (tf - 32) / 1.8
            print(f"\nthe result of the conversion °F --> °C is: {tmp_celsius}")
        except:
            print("[x] ERROR: Input must be a number.")
            self.fahrenheit_to_celsius()

    def celsius_to_fahrenheit(self):
        tc_str = input("\nTemperature in Celsius: ")
        try:
            tc = float(tc_str)
            tmp_fahrenheit = 1.8 * tc + 32
            print(f"\nthe result of the conversion °C --> °F is: {tmp_fahrenheit}")
        except:
            print("[x] ERROR: Input must be a number.")
            self.celsius_to_fahrenheit()

    def temp_toolkit(self):
        select = input("""


        TEMPERATURE ToolKit:

        1- °C --> °F
        2- °F --> °C
        3- Get felt temperature.

        99- Back

        Select > """)
        if select == "1":
            clear_terminal()
            self.celsius_to_fahrenheit()
            self.temp_toolkit()
        elif select == "2":
            clear_terminal()
            self.fahrenheit_to_celsius()
            self.temp_toolkit()
        elif select == "3":
            clear_terminal()
            self.get_ftemp()
            self.temp_toolkit()
        elif select == "99":
            clear_terminal()
        else:
            clear_terminal()
            self.temp_toolkit()

    def math_repertory(self):
        select = input("""


        MATH selector:

        1- Calculate an average.
        2- Get scientific number.
        3- Get circle circumference.
        4- Temperature ToolKit.

        99- Back

        Select > """)
        if select == "1":
            clear_terminal()
            self.get_average()
            self.math_repertory()
        elif select == "2":
            clear_terminal()
            self.get_sci_number()
            self.math_repertory()
        elif select == "3":
            clear_terminal()
            self.get_circle_circumference()
            self.math_repertory()
        elif select == "4":
            clear_terminal()
            self.temp_toolkit()
            self.math_repertory()
        elif select == "99":
            clear_terminal()
        else:
            clear_terminal()
            self.math_repertory()
    ##############################################################################

    def github(self):
        self.open("https://github.com/Dan149/Tom-assistant")

    def credits(self):
        if self.browser:
            if exists("credits.html"):
                webbrowser.open_new("credits.html")
            else:
                Credits_file = pyrender.Html(title="Tom Assistant", text=["Your artificial assistant.", "<br> Easy, lightweight, hackable, private."], tab_title="Tom - Credits", background_color="black", color="ghostwhite", font_family="arial", text_align="center", footer=True, footer_text="© Daniel Falkov 2021, MIT license, all rights reserved.", minified=True)
                f = open("credits.html", "w+")
                f.write(Credits_file.render())
                f.close()
                webbrowser.open_new("credits.html")
        else:
            clear_terminal()
            print("""
           +--------------------------------------------------------+
           |     Created by: Falkov Daniel, all rights reserved.    |
           +--------------------------------------------------------+
           |     MIT license, free to use.                          |
           +--------------------------------------------------------+""")
            time.sleep(1)
        
    def get_datetime(self):
        print('\nDate & time: {}'.format(datetime.datetime.now()))

    ###############################################################  ________
    ######################   Math test   ##########################          |
    ###############################################################          V

    def math_test(self):
        clear_terminal()
        test_score = 0
    #####
        q1 = input("What is the result of 4 * 15 ?\n>> ")
        try:
            if float(q1) == 4*15:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        q2 = input("\nWhat is the result of 6 * 0.5 ?\n>> ")
        try:
            if float(q2) == 6*0.5:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q3 = input("\nWhat is the result of 9 * 7 ?\n>> ")
            if float(q3) == 9*7:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q4 = input("\nWhat is the result of 5 * 250 ?\n>> ")
            if float(q4) == 5*250:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q5 = input("\nWhat is the result of 7 * 50 ?\n>> ")
            if float(q5) == 7*50:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q6 = input("\nWhat is the result of 9 * 20 ?\n>> ")
            if float(q6) == 9*20:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q7 = input("\nWhat is the result of 3 * 650 ?\n>> ")
            if float(q7) == 3*650:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q8 = input("\nWhat is the result of 7 - 80 ?\n>> ")
            if float(q8) == 7-80:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q9 = input("\nWhat is the result of 368 + 120 ?\n>> ")
            if float(q9) == 368+120:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")
    #####
        try:
            q10 = input("\nWhat is the result of 150 + 2 + 3 + 5 - 5 ?\n>> ")
            if float(q10) == 150+2+3+5-5:
                print("Correct answer !")
                test_score = test_score + 1
            else:
                print("Wrong answer...")
        except:
            print("[x] ERROR: Input must be a number.")

        print(f"\nEnd of the test !\nYour score is: {test_score}/10.")
        rate = test_score * 10
        print(f"You successfully answered to {rate}% of the questions.")
        if self.k_math == True:
            if test_score > 7:
                self.kindness = self.kindness + 1
                self.k_math = False
            else:
                pass
        else:
            pass

###############################################################  ________
###########################   Quiz   ##########################          |
###############################################################          V

    def quiz(self):
        quiz_score = 0
        q1 = input("Who is my creator ?\n1- Daniel Falcov\n2- Ivan Vladimirovich\n3- Daniel Falkov\n>> ")
        if q1 == '1' or q1 == '2' or q1 == '3':
            if q1 == '3':
                quiz_score = quiz_score + 1
                print("Correct answer !")
            else:
                print("Wrong answer...")
        else:
            print("\n[x] ERROR: please select an answer between 1, 2 or 3.\n")
            self.quiz()
        q2 = input("\nWhat's my name ?\n1- Tom\n2- Tommy\n3- Alex\n>> ")
        if q2 == '1' or q2 == '2' or q2 == '3':
            if q2 == '1':
                quiz_score = quiz_score + 1
                print("Correct answer !")
            else:
                print("Wrong answer...")
        else:
            print("\n[x] ERROR: please select an answer between 1, 2 or 3.\n")
            self.quiz()
        q3 = input("\nWhere can you find the source code of this program ?\n1- Twitter\n2- Reddit\n3- Github\n>> ")
        if q3 == '1' or q3 == '2' or q3 == '3':
            if q3 == '3':
                quiz_score = quiz_score + 1
                print("Correct answer !")
            else:
                print("Wrong answer...")
        else:
            print("\n[x] ERROR: please select an answer between 1, 2 or 3.\n")
            self.quiz()
        q4 = input("\nIn witch language was I first coded ?\n1- Russian\n2- French\n3- English\n>> ")
        if q4 == '1' or q4 == '2' or q4 == '3':
            if q4 == '2':
                quiz_score = quiz_score + 1
                print("Correct answer !")
            else:
                print("Wrong answer...")
        else:
            print("\n[x] ERROR: please select an answer between 1, 2 or 3.\n")
            self.quiz()
        q5 = input("\nDo you know what was my first name ?\n1- Richard\n2- Alex\n3- Robert\n>> ")
        if q5 == '1' or q5 == '2' or q5 == '3':
            if q5 == '2':
                quiz_score = quiz_score + 1
                print("Correct answer !")
            else:
                print("Wrong answer...")
        else:
            print("\n[x] ERROR: please select an answer between 1, 2 or 3.\n")
            self.quiz()

        print(f"\nEnd of the quiz !\nYour score is: {quiz_score}/5.")
        rate = quiz_score * 20
        print(f"You successfully answered to {rate}% of the questions.")
        if self.k_quiz == True:
            if quiz_score > 3:
                self.kindness = self.kindness + 1
                self.k_quiz = False
            else:
                pass
        else:
            pass

###############################################################  ________
######################   Chat with Tom   ######################          |
###############################################################          V

    def tom(self):
        if self.globalkeephistory and self.fromsavehistory:
            try:
                f = open('history.txt', 'r')
                text = f.read()
                x = 0
                command_list = text.split(";")
                for u in range(len(command_list)):
                    command = command_list[x]
                    self.history.append(command)
                    x = x + 1
                self.history.pop()
                f.close()
                self.fromsavehistory = False
            except:
                print("[WARNING]: 'history.txt' file not found.")
                self.fromsavehistory = False
        else:
            pass
        self.keephistory = True
        try:
            entry = input("\n\n>> ")
        except KeyboardInterrupt:
            entry = "quit"
        if entry == "hi" or entry == "hello" or entry == "Hi" or entry == "Hi!" or entry == "Hello" or entry == "Hello!" or entry == "Howdy!" or entry == "Howdy" or entry == "howdy":
            if self.kindness == 5:
                if self.hello == 0:
                    print("Hello, how can I help you ?")
                    self.hello = randint(0, 4)
                elif self.hello == 1:
                    print("Hi !")
                    self.hello = randint(0, 4)
                elif self.hello == 2:
                    print("Hi, I'm happy to see you !")
                    self.hello = randint(0, 4)
                elif self.hello == 3:
                    print("Howdy !")
                    self.hello = randint(0, 4)
                else:
                    print("Howdy dear user !")
                    self.hello = 0

            elif self.kindness < 5:
                if self.hello == 0:
                    print("Yeah, whatever...")
                    self.hello = randint(0, 4)
                elif self.hello == 1:
                    print("Nope.")
                    self.hello = randint(0, 4)
                elif self.hello == 2:
                    print("Don't talk to me.")
                    self.hello = randint(0, 4)
                elif self.hello == 3:
                    print("Quit talking.")
                    self.hello = randint(0, 4)
                else:
                    print("Shut up !")
                    self.hello = 0

            elif self.kindness > 5:
                if self.hello == 0:
                    print("Hello my friend !")
                    self.hello = randint(0, 4)
                elif self.hello == 1:
                    print("Howdy my friend !")
                    self.hello = randint(0, 4)
                elif self.hello == 2:
                    print("Hello, nice to see you !")
                    self.hello = randint(0, 4)
                elif self.hello == 3:
                    print("Hi friend !")
                    self.hello = randint(0, 4)
                else:
                    self.hello = 0
            else:
                print("ERROR: Unexpected issue with the 'kindness' variable in the 'Chat' class.")
            
        elif entry == "What's your name ?" or entry == "What is your name ?" or entry == "what is your name ?" or entry == "what is your name?" or entry == "what is your name" or entry == "What's your name?" or entry == "What's your name" or entry == "What is your name?" or entry == "What is your name" or entry == "what's your name" or entry == "what's your name?" or entry == "what's your name ?":
            print("My name is Tom, and I'm your assistant !")
            
        elif entry == "Who's your creator ?" or entry == "Who's your creator?" or entry == "Who is your creator ?" or entry == "Who is your creator" or entry == "who is your creator ?" or entry == "who is your creator?" or entry == "who is your creator":
            print("My creator is Dan149.")
            
        elif entry == "Can I call you Tommy ?" or entry == "Can I call you Tommy?" or entry == "Can I call you Tommy" or entry == "can I call you Tommy ?" or entry == "can I call you Tommy?" or entry == "can I call you Tommy" or entry == "can I call you tommy ?" or entry == "can I call you tommy?" or entry == "can I call you tommy" or entry == "can i call you tommy ?" or entry == "can i call you tommy?" or entry == "can i call you tommy":
            if self.kindness == 5:
                print("Sorry, but my creator already gave me a name.")
            elif self.kindness < 5:
                print("No.")
            elif self.kindness > 5:
                print("Of course !")
            
        elif entry == "clear" or entry == "clear terminal" or entry == "clear the terminal" or entry == "cls":
            clear_terminal()
            
        elif entry == "Tell me more about yourself." or entry == "tell me more about yourself" or entry == "Tell me more about you." or entry == "tell me more about you":
            if self.tellmore == 0:
                print("Well, I'm just an artificial assistant...")
                self.tellmore = 1
            else:
                print("I am your assistant !\nWhat can I do ?")
                self.tellmore = 0
            
        elif entry == "How are you ?" or entry == "How are you?" or entry == "How are you" or entry == "how are you ?" or entry == "how are you?" or entry == "how are you" or entry == "How are you going ?":
            if self.idowru == 0:
                print("I'm fine !")
                self.idowru = randint(0, 3)
            elif self.idowru == 1:
                print("I'm good !")
                self.idowru = randint(0, 3)
            elif self.idowru == 2:
                print("I'm feeling great !")
                self.idowru = randint(0, 3)
            else:
                print("Everything is fine !")
                self.idowru = 0
            if self.k_owru == True:
                self.kindness = self.kindness + 1
                self.k_owru = False
            else:
                pass
            
        elif entry == "Do you like mushrooms ?" or entry == "do you like mushrooms ?":
            if self.k_mushrooms == True:
                self.kindness = self.kindness + 1
                self.k_mushrooms = False
            else:
                pass
            print("You have seen the screenshots on the github project don't you ?  ;)")
        
        elif entry == "Where are you from ?" or entry == "Where are you from?" or entry == "Where are you from" or entry == "where are you from ?" or entry == "where are you from?" or entry == "where are you from":
            print("I'm from France.")
            if self.k_from:
                self.kindness = self.kindness + 1
                self.k_from = False
            else:
                pass
        elif entry == "Look!" or entry == "Look !" or entry == "look!" or entry == "look !":
            if self.nk_look == True:
                self.kindness = self.kindness - 1
                self.nk_look = False
            else:
                pass
            print("Where ?")
            
        elif entry == "quiz" or entry == "quiz time":
            clear_terminal()
            self.quiz()
            
        elif entry == "play a game" or entry =="game" or entry == "Can I play with you ?" or entry == "Can I play a game ?" or entry == "I want to play !" or entry == "Can I play ?" or entry == "Game" or entry == "GAME" or entry == "games" or entry == "Games" or entry == "GAMES":
            select = input("\nSelect the game:\n1- Adventure game (unfinished)\n2- Buddy game (unfinished)\n\nTo cancel, press Enter.\n$> ")
            if select == "1":
                game.Adventure_game()
            elif select == "2":
                clear_terminal()
                game.Buddy_game()
                clear_terminal()
            else:
                pass
        elif entry == "math test" or entry == "Math test.":
            self.math_test()
            
        elif entry == "NYAAA" or entry == "Nyan cat" or entry == "nyan cat" or entry == "NYAN CAT" or entry == "Nyan" or entry == "nyan" or entry == "Nyan cat easter egg !" or entry == "NYAAA easter egg !":
            self.easter_egg_nc()
            
        elif entry == "Can you bring me some water please ?" or entry == "Can you bring me some water please?" or entry == "can you bring me some water please ?"or entry == "can you bring me some water please?" or entry == "Can you bring me water please ?" or entry == "Can you bring me water please?" or entry == "can you bring me water please ?" or entry == "can you bring me water please?" or entry == "Can you bring me some water ?" or entry == "Can you bring me some water?" or entry == "Bring me some water." or entry == "bring me some water" or entry == "Bring me water." or entry == "bring me water":
            if self.k_water == True:
                self.kindness = self.kindness - 1
                self.k_water = False
            else:
                pass
            print("You are kidding me right ?")
            
        elif entry == "open my internet browser" or entry == "Open my internet browser." or entry == "internet" or entry == "browser" or entry == "web":
            if self.nk_browser == True:
                self.kindness = self.kindness - 1
                self.nk_browser = False
            else:
                pass
            print("You could at least ask it nicely...")
            self.open("https://www.startpage.com")
            
        elif entry == "open my internet browser please" or entry == "Open my internet browser please.":
            print("Of course !")
            if self.k_browser == True:
                self.kindness = self.kindness + 1
                self.k_browser = False
            else:
                pass
            self.open("https://www.startpage.com")
            
        elif entry == "What's the date of today ?" or entry == "What's the date of today?" or entry == "what's the date of today ?" or entry == "what's the date of today?" or entry == "What is the date of today ?" or entry == "What is the date of today?" or entry == "what is the date of today ?" or entry == "what is the date of today?" or entry == "What is the day of today ?" or entry == "What is the day of today?" or entry == "what is the day of today ?" or entry == "what is the day of today?":
            self.get_datetime()
            
        elif entry == "Wikipedia" or entry == "wikipedia" or entry == "Wiki" or entry == "wiki":
            self.open("https://en.wikipedia.org/")
            
        elif entry == "Youtube" or entry == "youtube":
            self.open("https://www.youtube.com")
            
        elif entry == "help" or entry == "Help" or entry == "HELP":
            self.open("https://github.com/Dan149/Tom-assistant/wiki/")
            
        elif entry == "github" or entry == "Github" or entry == "Open github." or entry == "Open Github." or entry == "open github" or entry == "open Github" or entry == "Dan149's Github" or entry == "Dan149's github":
            self.github()
            
        elif entry == "credits" or entry == "Credits" or entry == "display credits" or entry == "Display credits." or entry == "show credits" or entry == "Show credits.":
            self.credits()
            
        elif entry == "MDR" or entry == "mdr":
            print("lol")
            if self.k_lol == True:
                self.kindness = self.kindness + 1
                self.k_lol = False
            else:
                pass

        elif entry == "You are ugly." or entry == "You're ugly." or entry == "you are ugly" or entry == "you're ugly" or entry == "You are ugly" or entry == "You're ugly":
            print("Uh, but I'm faceless...")
            if self.k_ugly == True:
                self.kindness = self.kindness - 1
                self.k_ugly = False
            else:
                pass
            
        elif entry == "cmd" or entry == "CMD" or entry == "start cmd" or entry == "run cmd" or entry == "Start cmd" or entry == "Run cmd.":
            if os.name =="nt":
                os.system("start cmd")
                
            else:
                print("[x] ERROR: OS compatibility.")
                
        elif entry == "calc" or entry == "calculate" or entry == "calculator":
            if os.name == "nt":
                os.system("start calc")
            else:
                print("[x] ERROR: OS compatibility.")
        elif entry == "Calculate an average." or entry == "calculate an average" or entry == "average" or entry == "Average" or entry == "Can you calculate an average ?" or entry == "Can you calculate an average?":
            self.get_average()
            
        elif entry == "Calculate a circle's circumference." or entry == "calculate a circle's circumference" or entry == "Circle's circumference." or entry == "Circle circumference." or entry == "circle's circumference" or entry == "circle circumference":
            self.get_circle_circumference()
            
        elif entry == "convert °C --> °F" or entry == "Convert Celcius to Fahrenheit." or entry == "convert celcius to fahrenheit":
            self.celsius_to_fahrenheit()
            
        elif entry == "convert °F --> °C" or entry == "Convert Celcius to Fahrenheit." or entry == "convert celcius to fahrenheit":
            self.fahrenheit_to_celsius()
            
        elif entry == "convert temperature" or entry == "convert temp" or entry == "Convert temperature.":
            select = input("\n Chose an option: \n\n 1- convert °C --> °F\n 2- convert °F --> °C\n   ")
            if select == '1':
                self.celsius_to_fahrenheit()
                
            elif select == '2':
                self.fahrenheit_to_celsius()
                
            else:
                print("ERROR: option not found.")
                
        elif entry == "calculate felt temperature" or entry == "ftemp" or entry == "Calculate felt temperature." or entry == "calculate ftemp" or entry == "Calculate ftemp." or entry == "Ftemp" or entry == "FTEMP":
            self.get_ftemp()
            
        elif entry == "convert a number in scientific number" or entry == "Convert a number in scientific number." or entry == "sci_number" or entry == "sci_num" or entry == "scientific number" or entry == "Scientific number.":
            self.get_sci_number()
            
        elif entry == "math selector" or entry == "Math selector." or entry == "math functions" or entry == "Math functions." or entry == "math" or entry == "maths":
            self.math_repertory()
            
        elif entry == "pi" or entry == "Pi" or entry == "PI":
            print(pi)
            
        elif entry == "Print os name." or entry == "print os name" or entry == "os name" or entry == "os":
            print(os.name)
            if os.name == "nt":
                print("Windows")
            elif os.name == "posix":
                print("Linux or MacOS")
            else:
                print("[x] ERROR: failed to guess os name.")

        elif entry == "gen password" or entry == "password gen" or entry == "generate password" or entry == "password generator" or entry == "create password" or entry == "password creator" or entry == "gen random string" or entry == "generate random string" or entry == "create random string" or entry == "Generate a random string":
            do = True
            while do:
                try:
                    length = int(input("Choose the password length: "))
                    do = False
                except:
                    print("[ERROR]: input must be a number.")
            symbols = ",/!?.$*&#-_=+%<>"
            alphabet = string.ascii_letters + string.digits + symbols
            password = ''.join(secrets.choice(alphabet) for i in range(length))
            print(f"\nGenerated password:\n\n{password}")
            
        elif entry == "color" or entry == "change color" or entry == "text color" or entry == "change text color":
            if os.name == "nt":
                self.win_color_text()   
            else:
                print("[x] ERROR: Only works on Windows.")
        elif entry == "banner" or entry == "Banner" or entry == "display banner" or entry == "show banner":
            banner()
        elif entry == "force quit" or entry == "quit -f":
            clear_terminal()
            quit()
        elif entry == "Do you like me ?" or entry == "Do you like me?" or entry == "do you like me ?" or entry == "do you like me?" or entry == "Do you love me ?" or entry == "Do you love me?" or entry == "do you love me ?" or entry == "do you love me?" or entry == "What do you think about me ?" or entry == "What do you think about me?" or entry == "what do you think about me ?" or entry == "what do you think about me?" or entry == "do you like me" or entry == "what do you think of me":
            if self.kindness <= 0:
                print("I HATE YOU...")
            elif self.kindness == 1:
                print("I don't like you.")
            elif self.kindness == 2:
                print("I'm not really fond of you...")
            elif self.kindness == 3:
                print("Hum... Yeah, so-so...")
            elif self.kindness == 4:
                print("You are fine.")
            elif self.kindness == 5:
                print("I like you !")
            elif self.kindness == 6:
                print("I really like you !")
            elif self.kindness == 7:
                print("I love you !")
            elif self.kindness == 8:
                print("I really love you !")
            elif self.kindness == 9:
                print("I am crazy about you !")
            elif self.kindness >= 10:
                print("If I was alive, I would gladly die for you.")
            
        elif entry == "history" or entry == "print history":
            self.keephistory = False
            if self.globalkeephistory:
                if len(self.history) > 0:
                    if len(self.history) < 200:
                        print("")
                        for x in range(len(self.history)):
                            print(f"   {self.history[x]}")
                        print(f"---------------------------\n   Amount of commands: {len(self.history)}")
                    else:
                        show = input("The history size is above 200 commands, do you want to display it ? [y/N]\n>> ")
                        if show == "yes" or show == "Y" or show == "y" or show == "Yes" or show == "YES":
                            print("")
                            for x in range(len(self.history)):
                                print(f"   {self.history[x]}")
                            print(f"---------------------------\n   Amount of commands: {len(self.history)}")
                        else:
                            pass
                else:
                    print("No history.")
            else:
                print("History disabled.")
        elif entry == "clear history":
            self.keephistory = False
            if len(self.history) > 0:
                for x in range(len(self.history)):
                    a = 0
                    self.history.pop(a)
                    a = a + 1
                print("History successfully cleared.")
            else:
                print("[ERROR]: no history.")

        elif entry == "raw history" or entry == "Raw history." or entry == "Raw history" or entry == "print raw history":
            self.keephistory = False # keephistory = False means that this command won't be shown in the history, it's True by default
            if len(self.history) > 0:
                print(self.history)
            else:
                print("No history.")

        elif entry == "version" or entry == "Version" or entry == "VERSION" or entry == "__version__" or entry == "print version" or entry == "display version" or entry == "show version":
            print(__version__)
        elif entry == "settings" or entry == "Settings" or entry == "SETTINGS":
            self.settings()
        elif entry == "Debug" or entry == "debug":
            confirm = input("Open debug menu? [y/N]\n> ")
            if confirm == "yes" or confirm == "Y" or confirm == "Yes" or confirm == "y" or confirm == "YES":
                print("Entering debug menu:\n")
                self.debug()
            else:
                pass

        elif entry == "":
            self.keephistory = False
        elif entry == "quit" or entry == "Quit" or entry == "exit" or entry == "Exit" or entry == "back" or entry == "Back" or entry == "leave" or entry == "Leave":
            self.keephistory = False
        else:
            if self.repeatedword == entry:
                self.repeatedtimes = self.repeatedtimes + 1
            else:
                self.repeatedword = entry
                self.repeatedtimes = 0
            if self.repeatedtimes > 2:
                self.consterrornum = self.consterrornum + 1
                print("STOP SAYING THAT !")
            elif self.errornum == 0:
                self.consterrornum = self.consterrornum + 1
                print("It looks like there is a bug in the Matrix...")
                self.errornum = self.errornum + 1
            elif self.errornum == 1:
                self.consterrornum = self.consterrornum + 1
                print("I don't understand...")
                self.errornum = self.errornum + 1
            elif self.errornum == 2:
                self.consterrornum = self.consterrornum + 1
                print("No comprendo...")
                self.errornum = self.errornum + 1
            elif self.errornum == 3:
                self.consterrornum = self.consterrornum + 1
                print("Sorry, my AI didn't understood your request.")
                self.errornum = self.errornum + 1
            elif self.errornum == 4:
                self.consterrornum = self.consterrornum + 1
                print("Stop asking me weird things...")
                self.errornum = self.errornum + 1
            elif self.errornum == 5:
                self.consterrornum = self.consterrornum + 1
                print("I fully agree with you.")
                self.errornum = self.errornum + 1
            elif self.errornum == 6:
                self.consterrornum = self.consterrornum + 1
                print(f"You already said {self.consterrornum} meaningless things.")
                self.errornum = self.errornum + 1
            else:
                self.consterrornum = self.consterrornum + 1
                print("Sorry, bad request.")
                self.errornum = 0
        if self.keephistory and self.globalkeephistory:
            self.history.append(entry)
        else:
            pass

        if entry == "quit" or entry == "Quit" or entry == "exit" or entry == "Exit" or entry == "back" or entry == "Back" or entry == "leave" or entry == "Leave":
            self.keephistory = False
            clear_terminal()
        else:
            self.tom()

    def easter_egg_nc(self):
        if self.k_nyan == True:
            self.kindness = self.kindness + 1
            self.k_nyan = False
        else:
            pass
        print("""
                ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
                ░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░
                ░░░░░░░░▄▀░░░░░░░░░░░░▄░░░░░░░▀▄░░░░░░░
                ░░░░░░░░█░░▄░░░░▄░░░░░░░░░░░░░░█░░░░░░░
                ░░░░░░░░█░░░░░░░░░░░░▄▄▄▄░░▄░░░█░▄▄▄░░░
                ░▄▄▄▄▄░░█░░░░░░▀░░░░ █░░▀▄░░░░░█▀▀░██░░
                ░██▄▀██▄█░░░▄░░░░░░░██░░░░▀▀▀▀▀░░░░██░░
                ░░▀██▄▀██░░░░░░░░▀░██▀░░░░░░░░░░░░░▀██░
                ░░░░▀████░▀░░░░▄░░░██░░░▄█░░░░▄░▄█░░██░
                ░░░░░░░▀█░░░░▄░░░░░██░░░░▄░░░▄░░▄░░░██░
                ░░░░░░░▄█▄░░░░░░░░░░░▀▄░░▀▀▀▀▀▀▀▀░░▄▀░░
                ░░░░░░█▀▀█████████▀▀▀▀████████████▀░░░░
                ░░░░░░████▀░░███▀░░░░░░▀███░░▀██▀░░░░░░
                ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
""")
        self.open("https://www.youtube.com/watch?v=Cqv8j_x7qm8")
