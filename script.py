print("Initializing Tom...")
# -*- coding: utf-8 -*-
import tomodules
import community_scripts
# external modules:
import datetime
import webbrowser
import os
import time
import pathlib
from math import pi
import linecache

tomodules.clear_terminal()
try:
    printbanner = linecache.getline("settings.conf", 1)
except:
    printbanner = "startup_banner:True"
    print("[x]ERROR: failed to load 'settings.conf' with the linecache module, some features may not work.")
print(f"{tomodules.__version__} uploaded by the official github/gitlab project owner, DAN149/Daniel_fkv149.\n")
if printbanner == "startup_banner:True" or printbanner == "startup_banner:True\n" or printbanner == "startup_banner:True;" or printbanner == "startup_banner:True;\n":
    tomodules.banner()
else:
    pass

print("""\n               Welcome, I'm Tom !""")


def main():
    try:
        entry = input("""
                    > MENU <

        +---+-------------------------+
        | 1 | Chat with Tom (stable)  |
        +---+-------------------------+
        | Q | Quit                    |
        +-----------------------------+
\n      Select:
>> """)
    except KeyboardInterrupt:
        entry = "Q"
    if entry == '1':
        tomodules.clear_terminal()
        tomodules.Chat()
        main()
    elif entry == 'testbox':            # This function is located at the end of the 'tomodules.py' file.
        further = input("\nThis function is made for developers, do you want to continue ? [y/n]\n > ")
        if further == "y" or further == "Y" or further == "yes" or further == "Yes" or further == "YES":
            tomodules.clear_terminal()
            tomodules.testbox()
            input("Press Enter to continue...")
            tomodules.clear_terminal()
            main()
        else:
            tomodules.clear_terminal()
            main()
    elif entry == 'Q' or entry == "quit" or entry == "exit" or entry == "q" or entry == "Exit" or entry == "Quit":
        tomodules.exit()

    else:
        tomodules.clear_terminal()
        main()
main()
