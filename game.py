# -*- coding: utf-8 -*-
import os
from os import system as term
from time import sleep
from random import randint

def clear():
	if os.name == "nt":
		term("cls")
	else:
		term("clear")

class Adventure_game:
	key = False
	def __init__(self):
		clear()
		print("""
			WELCOME TO THE ADVENTURE GAME OF TOM ASSISTANT !

			Tip: only use lowercase letters
""")
		sleep(3)
		self.stage1(True)

	def back_to_tom(self):
		print("		Returning to the Tom Chat.\n")
		print("-------------------------------------------------------------------")
		sleep(3)
		clear()

	def stage1(self, intro): # the intro condition define if yes or not the introduction stage message will be shown
		if intro:
			act = input("\nYou wake up in a forest.\n$ ") # intro message
		else:
			act = input("\n$ ")

		if act == "look":
			print("\nYou see trees... and a house far away.")
			self.stage1(False)
		elif act == "house":
			print("\nYou slowly walk to the house...")
			self.stage2(True)
		else:
			print("Nope.")
			self.stage1(False)

	def stage2(self, intro):
		if intro:
			act = input("\nWhat do you do ?\n$ ")
		else:
			act = input("\n$ ")

		if act == "open the door" or act == "door" or act == "enter" or act == "enter in the house":
			print("\nYou open the door, the inside of the house is dark.\n")
			self.stage2b(True)
		else:
			print("Nope.")
			self.stage2(False)
	def stage2b(self, intro):
		if intro:
			act = input("\nWhat do you do ?\n$ ")
		else:
			act = input("\n$ ")

		if act == "light" or act == "switch on the light":
			print("\nYou switch on the light.\n")
			self.stage3(True)
		else:
			print("Nope.")
			self.stage2b(False)

	def stage3(self, intro):
		if intro:
			act = input("\nYou are in a room, you see a key.\n$ ")
		else:
			act = input("\n$ ")

		if act == "key" or act == "take the key" or act == "take a key":
			self.key = True
			print("You have a key !")
			self.stage3b(True)
		else:
			print("Nope.")
			self.stage3(False)

	def stage3b(self, intro):
		if intro:
			act = input("\nWhat do you do ?\n$ ")
		else:
			act = input("\n$ ")

		if act == "back" or act == "go back" or act == "get out" or act == "get out of the house" or act == "get back to the forest":
			print("\nYou go back to the forest, you can't find your way, you manage to find a river, you notice some water falls.\n")
			self.stage4(True)
		else:
			print("Nope.")
			self.stage3b(False)

	def stage4(self, intro):
		print("		UNDER DEVELOPMENT")
		self.back_to_tom()

######################################################################################

class Buddy_game:
	nullinput = False
	health = 5
	age = 0
	money = 50
	food = 0
	feedtimes = 0
	medics = 0
	day = 1
	hungry = False
	happy = True
	tired = False
	nosleep = False
	nowork = False
	sick = False
	ishungry = None  # yes/no (str)            *meant to be displayed on buddy status
	ishappy = None   # yes/no (str)            *meant to be displayed on buddy status
	ishealthy = None # low/medium/high (str)   *meant to be displayed on buddy status
	istired = None   # yes/no (str)            *meant to be displayed on buddy status
	issick = None	 # yes/no (str)            *meant to be displayed on buddy status

	baby_buddy = """ 
		 -----
		| * * |
		|  -  |
		 -----
		---|---
		   |
		  / \\
		 /   \\
"""
	baby_happy_buddy = """ 
		 -----
		| * * |
		|  v  |
		 -----
		---|---
		   |
		  / \\
		 /   \\
"""

	baby_tired_buddy = """ 
		 -----
		| - - |
		|  -  |
		 -----
		---|---
		   |
		  / \\
		 /   \\
"""

	adult_buddy = """
		 -------
		| _   _ |
		| o   o |
		|       |
		|  ---  |
		 -------
		    |
		   /|\\
		  / | \\
		 /  |  \\
		/   |   \\
		    |
		   / \\
		  /   \\
		 /     \\
		/       \\
"""

	adult_happy_buddy = """
		 -------
		| _   _ |
		| o   o |
		|       |
		|   U   |
		 -------
		    |
		   /|\\
		  / | \\
		 /  |  \\
		/   |   \\
		    |
		   / \\
		  /   \\
		 /     \\
		/       \\
"""

	adult_tired_buddy = """
		 -------
		| _   _ |
		| -   - |
		|       |
		|  ---  |
		 -------
		    |
		   /|\\
		  / | \\
		 /  |  \\
		/   |   \\
		    |
		   / \\
		  /   \\
		 /     \\
		/       \\
"""

	def __init__(self):
		self.buddy()
	def gameover(self):
		go = input("""
	   ______                        ____                  
	  / ____/___ _____ ___  ___     / __ \\_   _____  _____ 
	 / / __/ __ `/ __ `__ \\/ _ \\   / / / / | / / _ \\/ ___/ 
	/ /_/ / /_/ / / / / / /  __/  / /_/ /| |/ /  __/ / _ _ 
	\\____/\\__/_/_/ /_/ /_/\\___/   \\____/ |___/\\___/_(_|_|_)

Press Enter to quit the game...""")
		clear()
	def shop(self):
		while True:
			print(f"You have {self.money}$.")
			buy = input("""
	SHOP:
			1- Buy Food (5$)
			2- Medics (30$)

			B- Back
% """)
			if buy == "1":
				if self.money >= 5:
					clear()
					print("You bought 1 food (-5$).")
					self.money = self.money - 5
					self.food = self.food + 1
				else:
					clear()
					print("Not enough money, you should work.")
			if buy == "2":
				if self.money >= 30:
					clear()
					print("You bought 1 medics (-30$).")
					self.money = self.money - 30
					self.medics = self.medics + 1
				else:
					clear()
					print("Not enough money, you should work.")
			elif buy == "B":
				clear()
				break
			else:
				clear()

	def buddy(self):
		while True:
			if self.health == 0:
				self.gameover()
				break
			else:
				pass
			if not self.nullinput:
				setsick = randint(1,100)
				if setsick >= 90:
					self.sick = True
				else:
					pass
			else:
				self.nullinput == False
			if self.health > 3:
				self.ishealthy = "high"
			elif self.health == 3:
				self.ishealthy = "medium"
			else:
				self.ishealthy = "low"
			if self.tired:
				self.istired = "yes"
			else:
				self.istired = "no"
			if self.happy:
				if not self.tired:
					if self.feedtimes > 15 and self.day > 15:
						print(self.adult_happy_buddy)
					else:
						print(self.baby_happy_buddy)
				else:
					pass
				self.ishappy = "yes"
			else:
				if not self.tired:
					if self.feedtimes > 20 and self.day > 20:
						print(self.adult_buddy)
					else:
						print(self.baby_buddy)
				else:
					pass
				self.ishappy = "no"
			if self.tired:
				if self.feedtimes > 20 and self.day > 20:
					print(self.adult_tired_buddy)
				else:
					print(self.baby_tired_buddy)
			else:
				pass
			if self.hungry:
				self.ishungry = "yes"
			else:
				self.ishungry = "no"
			if self.sick:
				self.issick = "yes"
			else:
				self.issick = "no"
			
			menu = input(f"""
		ITEMS:
				Food: {self.food} | Medics: {self.medics}
				Money: {self.money}$
		-------------------------------------------------
		STATUS:
				Happy: {self.ishappy} | Tired: {self.istired}
				Hungry: {self.ishungry} | Health: {self.ishealthy}
				Day: {self.day} | sick: {self.issick}
		-------------------------------------------------
		MENU:
				1- feed buddy   2- go for a walk
				3- work         4- shop
				5- sleep	6- Use medics
				Q- Quit
	% """)
			if menu == "1":
				if self.food > 0:
					if self.hungry:
						clear()
						print("You feed your buddy.\nHe's not hungry anymore.")
						self.food = self.food - 1
						self.feedtimes = self.feedtimes + 1
						self.hungry = False
					else:
						clear()
						print("Your buddy isn't hungry.")
				else:
					clear()
					print("You don't have any food.")
			elif menu == "2":
				clear()
				if not self.tired:
					print("You go for a walk.")
					if not self.happy:
						self.happy = True
						clear()
						print("Buddy is now happy.")
					else:
						clear()
						print("Buddy is already happy.")
					if self.health > 4:
						self.health = self.health + 1
						if self.health == 5:
							clear()
							print("Buddy reached full health.")
						else:
							pass
					else:
						clear()
						print("Buddy already reached full health.")
					self.tired = True
					self.nosleep = False
				else:
					print("Your buddy is too tired to go for a walk.")
			elif menu == "3":
				clear()
				if not self.nowork:
					print("You went to your work, you earned 25$.")
					self.money = self.money + 25
					self.hungry = True
					self.happy = False
					self.nowork = True
				else:
					print("You already worked today.")
			elif menu == "4":
				clear()
				self.shop()
			elif menu == "5":
				clear()
				if not self.nosleep:
					print("Your budy slept, he's not tired anymore, but he's hungry.")
					if self.sick:
						self.health = self.health - 1
					else:
						pass
					self.tired = False
					self.hungry = True
					self.nosleep = True
					self.nowork = False
					self.day = self.day + 1
				else:
					print("Your buddy isn't tired.")
			elif menu == "6":
				if self.medics > 0:
					if self.sick:
						self.medics = self.medics - 1
						self.sick = False
						self.health = self.health + 1
						clear()
					else:
						clear()
						print("Your buddy isn't sick.")
				else:
					clear()
					print("No medics in inventory.")
			elif menu == "Q":
				break
			else:
				self.nullinput = True
				clear()
		else:
			print("Getting back to the Tom chat...")
